module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import CustomHttp
import Task
import Bootstrap.CDN as CDN
import Bootstrap.Grid as Grid
import Bootstrap.Card as Card
import Bootstrap.ButtonGroup as ButtonGroup
import Bootstrap.Button as Button
import Bootstrap.Modal as Modal
import Bootstrap.Form.Textarea as Textarea
import Types exposing (Task)
import ApiIn exposing (taskDecoder, tasksDecoder)
import ApiOut exposing (taskPostEncode, taskPatchEncode)


main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


baseTasksUrl =
    "http://138.68.85.157/api/tasks/"


singleTaskUrl : Int -> String
singleTaskUrl id =
    baseTasksUrl ++ (toString id) ++ "/"



-- MODEL


type alias Model =
    { tasks : List Task
    , taskParent : Maybe Int
    , taskNew : Bool
    , taskText : String
    , taskId : Int
    , editorState : Modal.State
    , getNumber : Int
    }


init : ( Model, Cmd Msg )
init =
    ( Model [] Nothing False "" 0 Modal.hiddenState 1
    , getTasks 0
    )



-- UPDATE


type Msg
    = PushTask
    | DeleteTask Int
    | GetTaskList
    | HandleTasksGet (Result Http.Error (List Task))
    | HandleTaskDelete (Result Http.Error ())
    | HandleTaskPatch (Result Http.Error ())
    | HandleTaskPost (Result Http.Error Task)
    | CreateTask (Maybe Int)
    | EditTask Task
    | ModalMsg Modal.State
    | EditorTextChange String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        EditorTextChange newText ->
            ( { model | taskText = newText }, Cmd.none )

        ModalMsg state ->
            ( { model | editorState = state }, Cmd.none )

        GetTaskList ->
            ( model, getTasks model.getNumber)

        HandleTasksGet (Ok tasks) ->
            ( { model | 
                  tasks = tasks,
                  getNumber = model.getNumber + 1}, Cmd.none )

        HandleTasksGet (Err _) ->
            ( {model | getNumber = model.getNumber + 1}, Cmd.none )

        DeleteTask id ->
            ( model, deleteTask id )

        HandleTaskDelete (Ok ()) ->
            (  model, getTasks model.getNumber)

        HandleTaskDelete (Err _) ->
            ( model, Cmd.none )

        PushTask ->
            case model.taskNew of
                True ->
                    ( model, (postTask model.taskText model.taskParent) )

                False ->
                    ( model, (patchTask model.taskText model.taskId) )

        HandleTaskPatch (Ok _) ->
            ( { model | editorState = Modal.hiddenState }
            , getTasks model.getNumber
            )

        HandleTaskPatch (Err _) ->
            ( model, Cmd.none )

        HandleTaskPost (Ok _) ->
            ( { model
                | editorState = Modal.hiddenState
              }
            , getTasks model.getNumber
            )

        HandleTaskPost (Err _) ->
            ( model, Cmd.none )

        CreateTask parent ->
            ( { model
                | taskText = ""
                , taskParent = parent
                , taskNew = True
                , editorState = Modal.visibleState
              }
            , Cmd.none
            )

        EditTask task ->
            ( { model
                | taskText = task.text
                , taskId = task.id
                , taskNew = False
                , editorState = Modal.visibleState
              }
            , Cmd.none
            )



-- VIEW


view : Model -> Html Msg
view model =
    Grid.container [] ([(editor model.editorState model.taskText), createButton] ++ (taskList model.tasks))


createButton : Html Msg
createButton =
    Card.config []
        |> Card.block []
            [ Card.custom
                (Button.button [ Button.primary, (Button.onClick (CreateTask Nothing)) ] [ text "Create new" ])
            ]
        |> Card.view


editor : Modal.State -> String -> Html Msg
editor editorState defaultText =
    Modal.config ModalMsg
        |> Modal.large
        |> Modal.h3 [] [ text "Task Editor" ]
        |> Modal.body []
            [ Textarea.textarea
                [ Textarea.rows 15
                , Textarea.value defaultText
                , Textarea.attrs [ onInput EditorTextChange ]
                ]
            ]
        |> Modal.footer []
            [ Button.button [ Button.primary, (Button.onClick PushTask) ]
                [ text "Save" ]
            ]
        |> Modal.view editorState

emptyNode = Html.text ""

isChild : Task -> Task -> Bool
isChild parent child =
    List.member child.id parent.children

taskList : List Task -> List (Html Msg)
taskList lst =
    List.map (renderTask lst) (List.filter (\x -> x.parent == Nothing) lst)

renderTask : List Task -> Task -> Html Msg
renderTask lst tsk =
    case tsk.children of
        [] -> task tsk
        _ ->
            taskWithChildren tsk (List.filter (isChild tsk) lst) lst


task : Task ->  Html Msg
task tsk =
    Card.config [ Card.attrs [class "media"] ]
        |> Card.block [] [ Card.titleH3 [] [ text (String.left 50 tsk.text) ] ]
        |> Card.block [] [ Card.text [] [ text ("Created: " ++ tsk.created) ] ]
        |> Card.block []
            [ Card.custom
                (ButtonGroup.buttonGroup []
                    [ ButtonGroup.button [ Button.primary, (Button.onClick (EditTask tsk)) ] [ text "Edit/View" ]
                    , ButtonGroup.button [ Button.danger, (Button.onClick (DeleteTask tsk.id)) ] [ text "Delete" ]
                    , ButtonGroup.button [ Button.success, (Button.onClick (CreateTask (Just tsk.id))) ] [ text "Create Subtask" ]
                    ]
                ) ]
        |> Card.view

taskWithChildren : Task -> List Task -> List Task ->  Html Msg
taskWithChildren tsk subtsks alltsks=
    Card.config [ Card.attrs [class "media"] ]
        |> Card.block [] [ Card.titleH3 [] [ text (String.left 50 tsk.text) ] ]
        |> Card.block [] [ Card.text [] [ text ("Created: " ++ tsk.created) ] ]
        |> Card.block []
            [ Card.custom
                (ButtonGroup.buttonGroup []
                    [ ButtonGroup.button [ Button.primary, (Button.onClick (EditTask tsk)) ] [ text "Edit/View" ]
                    , ButtonGroup.button [ Button.danger, (Button.onClick (DeleteTask tsk.id)) ] [ text "Delete" ]
                    , ButtonGroup.button [ Button.success, (Button.onClick (CreateTask (Just tsk.id))) ] [ text "Create Subtask" ]
                    ]
                ) ]
        |> Card.block [] (List.map ((renderTask alltsks) >> Card.custom) subtsks) 
        |> Card.view



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- HTTP


getTasks : Int -> Cmd Msg
getTasks num =
    Http.send HandleTasksGet (Http.get (baseTasksUrl ++ "?try=" ++ (toString num) ++ "/")  tasksDecoder)


deleteTask : Int -> Cmd Msg
deleteTask id =
    Http.send HandleTaskDelete (CustomHttp.delete (singleTaskUrl id))


patchTask : String -> Int -> Cmd Msg
patchTask newText id =
    Http.send HandleTaskPatch (CustomHttp.patch (singleTaskUrl id) (Http.jsonBody (taskPatchEncode newText)))


postTask : String -> Maybe Int -> Cmd Msg
postTask text parent =
    Http.send HandleTaskPost (Http.post baseTasksUrl (Http.jsonBody (taskPostEncode text parent)) taskDecoder)
