module ApiIn
    exposing
        ( tasksDecoder
        , taskDecoder
        )

import Json.Decode exposing (map5, field, int, string, nullable, list)
import Types exposing (Task)


taskDecoder =
    map5 Task
        (field "id" int)
        (field "text" string)
        (field "created" string)
        (field "parent" (nullable int))
        (field "children" (list int))


tasksDecoder =
    list taskDecoder
