module ApiOut exposing (taskPostEncode, taskPatchEncode)

import Types exposing (Task)
import Json.Encode exposing (Value, string, int, null, object)


nullableInt : Maybe Int -> Value
nullableInt val =
    case val of
        Just num ->
            (int num)

        Nothing ->
            null


taskPostEncode : String -> Maybe Int -> Value
taskPostEncode text parent =
    object
        [ ( "text", (string text) )
        , ( "parent", (nullableInt parent) )
        ]


taskPatchEncode : String -> Value
taskPatchEncode text =
    object
        [ ( "text", (string text) ) ]
