module Types exposing (Task)


type alias Task =
    { id : Int
    , text : String
    , created : String
    , parent : Maybe Int
    , children : List Int
    }
