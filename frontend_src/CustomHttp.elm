module CustomHttp exposing (delete, patch)

import Http exposing (..)


patch : String -> Body -> Request ()
patch url body =
    request
        { method = "PATCH"
        , headers = []
        , url = url
        , body = body
        , expect = expectStringResponse (\_ -> Ok ())
        , timeout = Nothing
        , withCredentials = False
        }


delete : String -> Request ()
delete url =
    request
        { method = "DELETE"
        , headers = []
        , url = url
        , body = emptyBody
        , expect = expectStringResponse (\_ -> Ok ())
        , timeout = Nothing
        , withCredentials = False
        }
