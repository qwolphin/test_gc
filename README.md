Приграмма поддерживает вложенные задачи, при этом при удалении задачи удаляются и все подзадачи. Также автоматически сохраняется время записи задачи. Формат отображения даты/времени наиболее примитивный.

Сервис был развёрнут на моём сервере с помощью Docker.

### Бекэнд работает на основе:
- Python 3
- Gunicorn
- Django
- Django REST framework
- SQLite
- nginx as reverse proxy

Предоставляет REST API, с которым уже работает фронтенд.

### Фронтенд работает на основе:
- Elm
- Bootstrap

### В данном коде допускаются следующие условности, делающие его не готовым для работы в промышленных условиях:
- отсутствие автоматического тестирования
- низкий уровень задокументированности (особенно у фронтенда)
- не приняты некоторые общие меры безопасности

В данном задании AngularJS был заменён на Elm, так как это позволило сделать его более быстро и качественно. 
Если необходим конкретно AngularJS и общая работа интересна, то возможно перевести на этот стек.
