from django.shortcuts import render

from core.models import Task
from core.serializers import TaskSerializer
from rest_framework import generics


class TaskList(generics.ListCreateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

class TaskItem(generics.RetrieveUpdateDestroyAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

