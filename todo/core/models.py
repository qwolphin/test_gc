"""
Django models from app "core"
"""
from django.db import models

class Task(models.Model):
  """
  Task
  """
  
  text = models.TextField()
  """Just task text"""

  created = models.DateTimeField(auto_now_add=True)
  """Creation date, autoset"""
  
  parent = models.ForeignKey("self", on_delete=models.CASCADE, null=True, related_name="children")
  """Optional task parent"""

  class Meta:
    ordering = ('created',)

