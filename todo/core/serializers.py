from rest_framework import serializers
from core.models import Task


class TaskSerializer(serializers.ModelSerializer):
    parent = serializers.PrimaryKeyRelatedField(queryset=Task.objects.all(), allow_null=True)
    children = serializers.PrimaryKeyRelatedField(read_only=True, many=True)
    class Meta:
        model = Task
        fields = ('id', 'text', 'created', 'parent', 'children')


